<?php

use App\Http\Controllers\FoodController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [FoodController::class,"index"])->name("list-food");
Route::get('/add', [FoodController::class,"inputIndex"])->name("add.food");
Route::post('/add', [FoodController::class,"input"])->name("add.food");
Route::get("{id}", [FoodController::class, "detail"])->name("food.detail");
Route::post("like", [FoodController::class, "like"])->name("food.like");
Route::post("query-searching", [FoodController::class, "searching"])->name("food.searching");


