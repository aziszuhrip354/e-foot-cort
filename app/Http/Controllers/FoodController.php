<?php

namespace App\Http\Controllers;

use App\Models\Food;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    public function index()
    {
        $foods = Food::all();
        return view("list-food", \compact("foods"));
    }

    public function inputIndex()
    {
        return view("form");
    }

    public function input(Request $request)
    {
        $name = $request->name;
        $desc = $request->desc;
        $price = $request->price;
        $priceReal = $request->price_real;
        $likes = $request->likes;
        $photo = $request->photo;

        $fileName = date("YmdHis").".".$photo->getClientOriginalExtension();
        $path = "image_products/{$fileName}";
        $photo->move("image_products", $fileName);

        $createNewFood = new Food();
        $createNewFood->description = $desc;
        $createNewFood->real_price = $priceReal;
        $createNewFood->price = $price;
        $createNewFood->total_likes = $likes;
        $createNewFood->path = $path;
        $createNewFood->title = $name;
        $createNewFood->save();

        return \redirect()->back();
    }

    public function detail($id)
    {
        $food = Food::find($id);
        if(!$food) {
            \abort(404);
        }
        return view("detail", \compact("food"));
    }

    public function like(Request $request)
    {
        $response = [
            "status" => true,
            "code" => 200,
        ];

        $id = $request->id;
        $food = Food::find($id);
        $food->total_likes+= 1;
        $food->update();
        $response["total_likes"] = $food->total_likes;

        return \response()->json($response ,$response["code"]);
    }

    public function searching(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => \false,
            "data_request" => $request->all(),
        ];



        $searching = $request->searching;
        $sorting = $request->sorting;

        if($searching) {
            $foods = Food::where("title", "ilike","%{$searching}%");
        } else {
            $foods = Food::selectRaw("*");
        }

        switch ($sorting) {
            case 'termurah':
                $orderBy = "ASC"; 
                $order = "price";
                break;
            case 'termahal':
                $orderBy = "DESC"; 
                $order = "price";
                break;
            case "palingdisukai":
                $orderBy = "DESC"; 
                $order = "total_likes";
                break;
            default:
                $orderBy = "ASC"; 
                $order = "id";
                break;
        }
        $foods = $foods->orderBy($order, $orderBy)->get();

        $response["html"] = view("foods", \compact("foods"))->render();

        return \response()->json($response, $response["code"]);
    }
}
