<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Input Makanan</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>
    <div class="app">
        <form class="" action="{{route('add.food')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Nama Makanan</label>
              <input type="text" name="name"  class="form-control">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Deskripsi Makanan</label>
                <textarea name="desc" id="" cols="30" class="form-control" rows="10"></textarea>
              </div>
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label">Harga Potongan</label>
              <input class="form-control" type="number" name="price">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Harga</label>
                <input class="form-control" type="number" name="price_real">
              </div>
              <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Total like</label>
                <input class="form-control" type="number" name="likes">
              </div>
            <input type="file" name="photo">
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>
</body>
</html>