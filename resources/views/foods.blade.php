@foreach ($foods as $food)
        <div class="col-md-6 my-2 container-food">
            <div class="contqainer-img w-100 images">
                <img src="{{asset($food->path)}}" class="w-100 rounded " alt="">
            </div>
            <div class="description w-100 foods">
                <div class="text-center my-2 t-small">
                    <small class="text-muted text-center w-100">❤️ {{$food->total_likes}} orang suka ini</small>
                </div>
                <p class="fw-bold m-0 px-4">{{$food->title}}</p>
                <div class="px-4">
                    <span class="fw-bold t-medium c-primary">Rp. {{number_format($food->price,2,',','.')}}</span>
                    <span class="fw-bold text-muted t-small"> <s>Rp. {{number_format($food->real_price,2,',','.')}}</s></span>
                </div>
                <div class="px-4 my-3">
                    <a href="{{route("food.detail", $food->id)}}" class="btn c-primary fw-bold w-100 b-primary">Detail</a>
                </div>
            </div>
        </div>
@endforeach