<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    

    <title>E-Food Cort</title>
  </head>
  <body class="w-100">
    <div id="app" class="">
        <h5 class="fw-bold text-center py-2 bg_primary text-white" id="title">E-Food Cort</h5>
        <div class="container p-3 px-4">
            <div class="row">
                <div class="row">
                    <div class="col-6">
                        <select name="sorting" id="sorting" onchange="searching();" class="w-100 b-primary c-primary btn fw-bold">
                            <option value="">Sorting</option>
                            <option value="termurah">Termurah</option>
                            <option value="termahal">Termahal</option>
                            <option value="palingdisukai">Terekomendasi</option>
                        </select>
                    </div>
                    <div class="col-6">
                        <div class="">
                            <input type="text" onkeyup="searching();" class="form-control b-primary c-primary py-2" id="searching" placeholder="Cari makanan......">
                          </div>
                    </div>
                </div>
                <div class="row my-2" id="listFoods">
                    @foreach ($foods as $food)
                        <div class="col-md-6 my-2 container-food">
                            <div class="contqainer-img w-100 images">
                                <img src="{{asset($food->path)}}" class="w-100 rounded " alt="">
                            </div>
                            <div class="description w-100 foods">
                                <div class="text-center my-2 t-small">
                                    <small class="text-muted text-center w-100">❤️ {{$food->total_likes}} orang suka ini</small>
                                </div>
                                <p class="fw-bold m-0 px-4">{{$food->title}}</p>
                                <div class="px-4">
                                    <span class="fw-bold t-medium c-primary">Rp. {{number_format($food->price,2,',','.')}}</span>
                                    <span class="fw-bold text-muted t-small"> <s>Rp. {{number_format($food->real_price,2,',','.')}}</s></span>
                                </div>
                                <div class="px-4 my-3">
                                    <a href="{{route("food.detail", $food->id)}}" class="btn c-primary fw-bold w-100 b-primary">Detail</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            }
        });

        const searching = () => {
            let sorting = $("#sorting").val();
            let searching = $("#searching").val();
            console.log(sorting, searching);
            $.ajax({
                url: '{{route('food.searching')}}',
                dataType: "json",
                type: "post",
                async: true,
                data: {sorting, searching},
                success: function (data) {
                    $("#listFoods").html(data.html)
                },  
        }); 
        }
    </script>
  </body>
</html>
