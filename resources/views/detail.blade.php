<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    

    <title>E-Food Cort</title>
  </head>
  <body class="w-100">
    <div id="app" class="">
        <h5 class="fw-bold text-center py-2 bg_primary text-white" id="title">E-Food Cort</h5>
        <div class="container p-3 px-4">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{asset($food->path)}}" class="w-100 rounded" alt="">
                </div>
                <div class="col-md-12 my-2">
                    <div class="d-flex w-100 align-items-center justify-content-between">
                        <small class="fw-bold text-muted" id="total_likes">
                            ❤️ {{$food->total_likes}} orang suka ini
                        </small>
                        <button class="btn" onclick="like(this);">
                            <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>
                        </button>
                    </div>


                    <h4 class="fw-bold my-2">{{$food->title}}</h4>
                    <div class="">
                        <span class="fw-bold c-primary">Rp. {{number_format($food->price,2,',','.')}}</span>
                        <span class="fw-bold text-muted"> <s>Rp. {{number_format($food->real_price,2,',','.')}}</s></span>
                    </div>

                    <div>
                        <h4 class="fw-bold my-2">Deskripsi</h4>
                        <span class="t-medium">{{$food->description}}</span>
                    </div>
                    <a href="{{route('list-food')}}" class="btn bg_primary py-2 text-white w-100 my-2">
                        kembali ke daftar makanan
                    </a>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

  </body>
  <script>
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            }
        });
    const like = (self) => {
        let id = '{{$food->id}}';
        $.ajax({
            url: '{{route('food.like')}}',
            dataType: "json",
            type: "Post",
            async: true,
            data: {id},
            success: function (data) {
                let el = "<h4 class='fw-bold'>❤️</h4>";
                $(self).html(el);
                $("#total_likes").html(`❤️ ${data.total_likes} orang suka ini`)
            },  
        }); 
    }
  </script>
</html>
